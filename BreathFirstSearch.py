grafica = {'A': set(['B', 'C']),
         'B': set(['A', 'D', 'E']),
         'C': set(['A', 'F']),
         'D': set(['B']),
         'E': set(['B', 'F']),
         'F': set(['C', 'E'])}

#Recorre toda la gráfica, no tiene objetivo, regresa todos los vértices visitados.
def bfs(grafica, inicio):
    visitado, cola = set(), [inicio]
    while cola:
        vertice = cola.pop(0)
        if vertice not in visitado:
            visitado.add(vertice)
            cola.extend(grafica[vertice] - visitado)
    return visitado

bfs(grafica, 'A') # {'B', 'C', 'A', 'F', 'D', 'E'}


# Recorre toda la gráfica y regresa un generador de los diferentes caminos posibles (paths)
def bfs_paths(grafica, inicio, objetivo):
    cola = [(inicio, [inicio])]
    while cola:
        print("Cola:{} ".format(cola))
        (vertice, path) = cola.pop(0)
        print("Vertice: {} Path: {}".format(vertice, path))
        for next in grafica[vertice] - set(path):
            if next == objetivo:
                yield path + [next]
            else:
                cola.append((next, path + [next]))

list(bfs_paths(grafica, 'A', 'F')) # [['A', 'C', 'F'], ['A', 'B', 'E', 'F']]

first_results = bfs_paths(grafica,'A','F')
print(list(first_results))

# Recorre toda la gráfica y regresa el camino (path más corto)
def shortest_path(grafica, inicio, objetivo):
    try:
        return next(bfs_paths(grafica, inicio, objetivo))
    except StopIteration:
        return None

shortest_path(grafica, 'A', 'F') # ['A', 'C', 'F']

